# Editorconfig plugin for [Proton](https://gitlab.com/raggesilver-proton/proton)

This is Proton's editorconfig plugin. For more information about what editorconfig is visit [https://editorconfig.org](https://editorconfig.org/).

## Credits

This plugin was initially based on Elementary's Code [editorconfig plugin](https://github.com/elementary/code/blob/master/plugins/editorconfig).
